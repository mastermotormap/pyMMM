%module pymmm

%{
	#include "MMM/Model/Model.h"
	#include "MMM/Model/ModelProcessor.h"
	#include "MMM/Model/ModelProcessorWinter.h"
	#include "MMM/Model/ModelReaderXML.h"
	#include "MMM/Motion/Legacy/LegacyMotion.h"
    #include "MMM/Motion/Legacy/MotionFrame.h"
    #include "MMM/Motion/Legacy/LegacyMotionReaderXML.h"
	#include "MMMSimoxTools/MMMSimoxTools.h"
%}

%include "std_string.i"
%include "std_vector.i"

namespace std {
    %template(StdVectorFloat) vector<float>;
    %template(StdVectorString) vector<std::string>;
}

%include "boost_shared_ptr.i"
%shared_ptr(MMM::LegacyMotion)
%shared_ptr(MMM::MotionFrame)
%shared_ptr(MMM::MotionFrameEntry)
%shared_ptr(MMM::Model)
%shared_ptr(MMM::ModelNode)
%shared_ptr(MMM::ModelProcessor)

%template(LegacyMotionPtr) boost::shared_ptr<MMM::LegacyMotion>;
%template(MotionFramePtr) boost::shared_ptr<MMM::MotionFrame>;
%template(MotionFrameEntryPtr) boost::shared_ptr<MMM::MotionFrameEntry>;
%template(ModelPtr) boost::shared_ptr<MMM::Model>;
%template(ModelNodePtr) boost::shared_ptr<MMM::ModelNode>;
%template(ModelProcessorPtr) boost::shared_ptr<MMM::ModelProcessor>;

%template(StdVectorMotionFramePtr) std::vector<boost::shared_ptr<MMM::MotionFrame> >;

%include <eigen.i>

%eigen_typemaps(Eigen::Vector3f)
%eigen_typemaps(Eigen::Matrix3f)
%eigen_typemaps(Eigen::Matrix4f)
%eigen_typemaps(Eigen::MatrixXf)

// Rename all methods called "print" to "print_instance" because print is a reserved keyword in Python
%rename(print_instance) print;

namespace VirtualRobot {
	class Robot;	
	typedef std::shared_ptr<Robot> RobotPtr;
}

namespace MMM {
	class LegacyMotion;
	typedef boost::shared_ptr<LegacyMotion> LegacyMotionPtr;

	class MotionFrame;
	typedef boost::shared_ptr<MotionFrame> MotionFramePtr;

	class MotionFrameEntry;
	typedef boost::shared_ptr<MotionFrameEntry> MotionFrameEntryPtr;

	class Model;
	typedef boost::shared_ptr<Model> ModelPtr;

	class ModelNode;
	typedef boost::shared_ptr<ModelNode> ModelNodePtr;

	class ModelProcessor;
	typedef boost::shared_ptr<ModelProcessor> ModelProcessorPtr;

	typedef std::vector<LegacyMotionPtr> LegacyMotionList;

	class SegmentInfo {
	public:
	        Eigen::Vector3f com;
	        float mass;
	        Eigen::Matrix3f inertia;
                std::string visuType;
               	std::string visuFile;
        };

	class ModelNode {
	public:
               	SegmentInfo segment;
        };
        %extend ModelNode { SegmentInfo getSegmentInfo() { return self->segment; } }

	class Model {
	public:
                ModelNodePtr getModelNode(const std::string& nodeName);
        };

	class LegacyMotionReaderXML {
	public:
		LegacyMotionReaderXML();
		LegacyMotionPtr loadMotion(const std::string &xmlFile, const std::string &motionName = std::string());
		LegacyMotionList loadAllMotions(const std::string &xmlFile);
		LegacyMotionList loadAllMotionsFromString(const std::string &xmlDataString);
		std::vector<std::string> getMotionNames(const std::string &xmlFile) const;
	};

	typedef boost::shared_ptr<LegacyMotionReaderXML> LegacyMotionReaderXMLPtr;

	class LegacyMotion {
	public:
		LegacyMotion(const std::string& name);
		LegacyMotion(const LegacyMotion& m);
		bool addMotionFrame(MotionFramePtr md);
		bool removeMotionFrame(size_t frame);
		bool setJointOrder(const std::vector<std::string> &jointNames);
		void setName(const std::string& name);
		std::string getName();
		void setComment(const std::string &comment);
		void addComment(const std::string &comment);
		std::string getComment();
		std::vector<std::string> getJointNames();
		MotionFramePtr getMotionFrame(size_t frame);
		std::vector<MotionFramePtr> getMotionFrames();
		void setMotionFrames(std::vector<MotionFramePtr> &mfs){motionFrames = mfs;}
		virtual unsigned int getNumFrames();
		void setModel(ModelPtr model);
		void setModel(ModelPtr processedModel, ModelPtr originalModel);
		ModelPtr getModel(bool processedModel = true);
		const std::string& getMotionFilePath();
		void setMotionFilePath(const std::string&filepath);
		const std::string& getMotionFileName();
		void setMotionFileName(const std::string&filename);
		void setModelProcessor(ModelProcessorPtr mp);
		ModelProcessorPtr getModelProcessor();
		virtual std::string toXML();
		void print();
		void print(const std::string &filename);
		bool hasJoint( const std::string &name );
		void calculateVelocities(int method = 0);
		void calculateAccelerations(int method = 0);
		enum jointEnum{eValues, eVelocities, eAccelerations};
		void smoothJointValues(jointEnum type, int windowSize);
		LegacyMotionPtr getSegmentMotion(size_t frame1, size_t frame2);
		LegacyMotionPtr copy();
		LegacyMotionList getSegmentMotions(std::vector<int> segmentPoints);
	};

	class MotionFrame {
	public:
		MotionFrame(unsigned int ndof);
		MotionFrame(const MotionFrame &inpSrc);
		Eigen::Matrix4f getRootPose();
		Eigen::Vector3f getRootPos();
		Eigen::Vector3f getRootPosVel();
		Eigen::Vector3f getRootPosAcc();
		Eigen::Vector3f getRootRot();
		Eigen::Vector3f getRootRotVel();
		Eigen::Vector3f getRootRotAcc();
		bool setRootPose(const Eigen::Matrix4f &pose);
		bool setRootPos(const Eigen::Vector3f &pos);
		bool setRootPosVel(const Eigen::Vector3f &posVel);
		bool setRootPosAcc(const Eigen::Vector3f &posAcc);
		bool setRootRot(const Eigen::Vector3f &rot);
		bool setRootRotVel(const Eigen::Vector3f &rotVel);
		bool setRootRotAcc(const Eigen::Vector3f &rotAcc);
		float timestep;
		Eigen::VectorXf joint;
		Eigen::VectorXf joint_vel;
		Eigen::VectorXf joint_acc;
		bool addEntry(const std::string &name, MotionFrameEntryPtr entry );
		bool removeEntry(const std::string &name);
		bool hasEntry(const std::string &name) const;
		MotionFrameEntryPtr getEntry(const std::string &name);
		std::string toXML();
		unsigned int getndof();
	};

	class ModelReaderXML {
	public:
		ModelReaderXML();
		ModelPtr loadModel(const std::string &xmlFile, const std::string& name = std::string());
	};

	class ModelProcessorWinter {
	public:
		ModelProcessorWinter();
		virtual ModelPtr convertModel(ModelPtr input);
		virtual bool setup(float height, float mass, float handLength = -1., float handWidth = -1.);
	};

	namespace SimoxTools {
		VirtualRobot::RobotPtr buildModel(MMM::ModelPtr model, bool loadVisualizations = true);
	}
}
